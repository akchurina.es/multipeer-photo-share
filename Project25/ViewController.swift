//
//  ViewController.swift
//  Project25
//
//  Created by Ekaterina Musiyko on 25/03/2020.
//  Copyright © 2020 Ekaterina Akchurina. All rights reserved.
//

import UIKit
import MultipeerConnectivity


class ViewController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MCBrowserViewControllerDelegate, MCSessionDelegate {
    
// MARK: - vars
    var peerID: MCPeerID!
    var mcsession: MCSession!
    var mcadvertiserassistant: MCAdvertiserAssistant!
    
    var images = [UIImage]()
//MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Selfie Share"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showConnectionPrompt))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(importPicture))
        
        peerID = MCPeerID(displayName: UIDevice.current.name)
        mcsession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        mcsession.delegate = self
//        mcadvertiserassistant?.delegate = self
    }
//MARK: - Collection View
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageView", for: indexPath)
        
        if let imageView = cell.viewWithTag(1000) as? UIImageView {
            imageView.image = images[indexPath.item]
        }
        return cell
    }
//MARK: - Add images from library
    @objc func importPicture(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {return}
        dismiss(animated: true)
        images.insert(image, at: 0)
        collectionView.reloadData()
        
//        sending image to other peers
//        guard let mcsession = mcsession else {return}
        if mcsession.connectedPeers.count > 0 {
            if let imageData = image.pngData() {
                do {
                    try mcsession.send(imageData, toPeers: mcsession.connectedPeers, with: .reliable)
                } catch let error as NSError {
                    let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    present(ac, animated: true)
                }
            }
        }
    }
// MARK: - Multipeer button
    @objc func showConnectionPrompt() {
          let ac = UIAlertController(title: "Connect to others", message: nil, preferredStyle: .alert)
          ac.addAction(UIAlertAction(title: "Host a session", style: .default, handler: startHosting))
          ac.addAction(UIAlertAction(title: "Join a session", style: .default, handler: joinSession))
          ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
          present(ac, animated: true)
      }
    
    func startHosting(_: UIAlertAction!){
//        guard let mcsession = mcsession else {return}
        mcadvertiserassistant = MCAdvertiserAssistant(serviceType: "hws-kb", discoveryInfo: nil, session: mcsession)
        mcadvertiserassistant.start()
    }
    
    func joinSession(_: UIAlertAction!){
//        guard let mcsession = mcsession else {return}
        let mcbrowser = MCBrowserViewController(serviceType: "hws-kb", session: mcsession)
        mcbrowser.delegate = self
        present(mcbrowser, animated: true, completion: nil)
        
    }
    
//    func advertiserAssistantWillPresentInvitation(_ advertiserAssistant: MCAdvertiserAssistant) {
//
//    }
//    func advertiserAssistantDidDismissInvitation(_ advertiserAssistant: MCAdvertiserAssistant) {
//
//    }
//    MARK: - Peer protocols stubs
//    first three empty ones - for purpose
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    
//what to do when work with mcbrowser is done
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true, completion: nil)
    }
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true, completion: nil)
    }
// diagnostics for connection
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            print("Connected \(peerID.displayName)")
        case .connecting:
            print("Connecting \(peerID.displayName)")
        case .notConnected:
            print("Not connected \(peerID.displayName)")
        @unknown default:
            print("Unknown state for \(peerID.displayName)")
        }
    }
//    important method: what to do with received data
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        DispatchQueue.main.async { [weak self] in
            if let image = UIImage(data: data) {
                self?.images.insert(image, at: 0)
                self?.collectionView.reloadData()
            }
        }
    }
}
